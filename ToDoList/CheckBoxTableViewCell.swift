//
//  CheckBoxCell.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/2/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import UIKit

public class CheckBoxCell: UITableViewCell {
    private lazy var titleCheckBox: CheckBoxView = {
        let checkBoxView = CheckBoxView()
        checkBoxView.translatesAutoresizingMaskIntoConstraints = false
        return checkBoxView
    }()

    func configure(task: Task, searchTerm: String, checkedChangedHandler: @escaping ((_ value: Bool) -> Void)) {

        if titleCheckBox.superview == nil {
            contentView.addSubview(titleCheckBox)
            NSLayoutConstraint.activate([
                titleCheckBox.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
                titleCheckBox.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
                titleCheckBox.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
                titleCheckBox.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),

            ])
        }

        titleCheckBox.configure(task: task, searchTerm: searchTerm) { (answer, value) in
            task.finishTask(isFinished: value)
            checkedChangedHandler(value)
        }
    }
}
