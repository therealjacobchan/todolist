//
//  RootCoordinator.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/1/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import UIKit

public class RootCoordinator {
    private var window: UIWindow?
    private let navigationController: NavigationController = NavigationController()

    func start(window: UIWindow?) {
        self.window = window

        window?.rootViewController = configureRoot()

        window?.makeKeyAndVisible()
    }

    private func configureRoot() -> UIViewController {
        let viewModel = ViewModel()
        let viewController = ViewController(viewModel: viewModel)
        navigationController.setViewControllers([viewController], animated: true)

        return navigationController
    }
}

public class NavigationController: UINavigationController {
    public override var prefersStatusBarHidden: Bool {
        return true
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
                navigationBar.tintColor = UIColor.black
                navigationBar.barTintColor = UIColor.white
            case .dark:
                navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                navigationBar.tintColor = UIColor.white
                navigationBar.barTintColor = UIColor.black
        @unknown default:
            break
        }

        navigationBar.barStyle = .black
        navigationBar.isTranslucent = true
    }
}
