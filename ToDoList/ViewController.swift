//
//  ViewController.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/1/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class ViewController: UIViewController {
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.isAccessibilityElement = true
        tableView.accessibilityIdentifier = "tableView"
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
        tableView.register(CheckBoxCell.self, forCellReuseIdentifier: "CheckBoxCell")
        tableView.separatorInset = .zero
        tableView.backgroundColor = .systemBackground
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = true
        return tableView
    }()

    private lazy var noDataView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false


        view.addSubview(self.textLabel)
        NSLayoutConstraint.activate([
            self.textLabel.topAnchor.constraint(equalTo: view.topAnchor),
            self.textLabel.bottomAnchor.constraint(equalTo:  view.bottomAnchor),
            self.textLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            self.textLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        return view
    }()

    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 40)
        label.numberOfLines = 0
        label.textColor = .systemGray
        label.text = "No tasks found"
        label.textAlignment = .center
        return label
    }()

    private lazy var right: UIBarButtonItem = {
        let right = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
        return right
    }()

    private let viewModel: ViewModel
    var disposeBag = DisposeBag()

    let searchController = UISearchController(searchResultsController: nil)

    public init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = UIRectEdge.top
        view.backgroundColor = .systemBackground

        view.addSubview(tableView)
        view.addSubview(noDataView)

        NSLayoutConstraint.activate([
            noDataView.topAnchor.constraint(equalTo: view.topAnchor),
            noDataView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            noDataView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            noDataView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])

        self.noDataView.isHidden = self.viewModel.getTasks().value.count != 0

        setupViewModel()
        setupNavigation()
        setupSearch()
        setupTableView()
    }

    private func setupViewModel() {
        viewModel.getTasksSuccessfully = { [weak self] (response, issue) in
            guard let `self` = self else { return }
            if !response {
                self.popError(title: "Warning", issue: issue)
            }
        }

        viewModel.modifyTaskSuccessfully = { [weak self] (response, issue) in
            guard let `self` = self else { return }
            if !response {
                self.popError(title: "Warning", issue: issue)
            }
            self.noDataView.isHidden = self.viewModel.getTasks().value.count != 0
        }
    }

    private func setupNavigation() {
        self.title = "To-do List"
        right.rx.tap.subscribe(
            onNext: { [weak self] in
                let alert = UIAlertController(title: "Add Task", message: "Please add task below", preferredStyle: .alert)
                alert.addTextField { [weak self] (textField) in
                    guard let _ = self else { return }
                    textField.placeholder = "Task Name"
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert, weak self] (_) in
                    guard let alert = alert else { return }
                    if let textFields = alert.textFields{
                        self?.viewModel.saveTask(task: Task(name: textFields[0].text ?? ""))
                    }
                }))
                self?.present(alert, animated: true, completion: nil)
            }
        ).disposed(by: disposeBag)

        self.navigationItem.rightBarButtonItem = right
    }

    private func setupTableView() {
        let observable = viewModel.getTasks().asObservable()

        observable.bind(to: tableView.rx.items(cellIdentifier: "CheckBoxCell", cellType: CheckBoxCell.self)) { [weak self] (row, element, cell) in
                guard let `self` = self else { return }
            cell.configure(task: element, searchTerm: self.viewModel.searchTerm) { [weak self] (result) in
                    guard let _ = self else { return }
                    element.finishTask(isFinished: result)
                }
        }.disposed(by: disposeBag)

        tableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                let task = self.viewModel.getTask(index: indexPath.row)
                let alert = UIAlertController(title: "Edit Task", message: "Please edit task below", preferredStyle: .alert)
                alert.addTextField { (textField) in
                    textField.placeholder = "Task Name"
                    textField.text = task.getName()
                }
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert, weak self] (_) in
                    guard let alert = alert else { return }
                    if let textFields = alert.textFields{
                        let task = Task(name: textFields[0].text ?? "")
                        self?.viewModel.editTask(index: indexPath.row, task: task)
                    }
                }))
                self.tableView.deselectRow(at: indexPath, animated: true)
                self.present(alert, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)

        tableView.rx.itemDeleted
            .subscribe(onNext : { [weak self] indexPath in
                let alert = UIAlertController(title: "WARNING", message: "Are you sure you want to delete this task?", preferredStyle: .actionSheet)
                let delete = UIAlertAction(title: "Delete", style: .destructive) { [weak self] (_) in
                    if let task = self?.viewModel.getTask(index: indexPath.row) {
                        self?.viewModel.deleteTask(task: task)
                    }
                }
                let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
                alert.addAction(delete)
                alert.addAction(cancel)
                self?.present(alert, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }

    private func setupSearch() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.showsCancelButton = false
        searchController.searchBar.placeholder = "Search task"
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            case .dark:
                UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        @unknown default:
            break
        }

        self.navigationItem.searchController = searchController
        searchController.searchBar
            .rx
            .text
            .orEmpty
            .distinctUntilChanged()
            .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [unowned self] query in
                self.viewModel.filterResults(text: query)
                self.noDataView.isHidden = self.viewModel.getTasks().value.count != 0
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = true
        }
    }

    private func popError(title: String, issue: String) {
        let alert = UIAlertController(title: title, message: issue, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}
