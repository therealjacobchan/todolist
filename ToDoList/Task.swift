//
//  Task.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/1/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import Foundation

public class Task: NSObject, NSCoding {
    public static func == (lhs: Task, rhs: Task) -> Bool {
        return true
    }

    private var name: String
    private var isFinished: Bool = false

    public init(name: String) {
        self.name = name
    }

    public func finishTask(isFinished: Bool) {
        self.isFinished = isFinished
    }

    public func getFinished() -> Bool {
        return isFinished
    }

    public func getName() -> String {
        return name
    }

    public func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name")
        coder.encode(self.isFinished, forKey: "isFinished")
    }

    public required init?(coder: NSCoder) {
        self.name = coder.decodeObject(forKey: "name") as? String ?? ""
        self.isFinished = coder.decodeBool(forKey: "isFinished")
    }
}
