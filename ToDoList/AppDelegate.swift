//
//  AppDelegate.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/1/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var rootCoordinator: RootCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        window = UIWindow(frame: UIScreen.main.bounds)
        rootCoordinator = RootCoordinator()
        rootCoordinator?.start(window: window)
        return true
    }
}

