//
//  CheckBoxView.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/1/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import UIKit

public class CheckBoxView: UIView {

    private lazy var checkBox: CheckBox = {
        let checkBox = CheckBox()
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        switch traitCollection.userInterfaceStyle {
            case .light, .unspecified:
                checkBox.uncheckedBorderColor = UIColor.black
            case .dark:
                checkBox.uncheckedBorderColor = UIColor.white
        @unknown default:
            break
        }
        checkBox.addTarget(self, action: #selector(onCheckBoxValueChange(_:)), for: .valueChanged)
        return checkBox
    }()

    private lazy var answerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0

        return label
    }()

    var checkedChangedHandler: ((_ task: Task, _ selected: Bool) -> Void)?
    var task: Task?
    var index: Int = -1

    func configure(task: Task, searchTerm: String, checkedChangedHandler: @escaping ((_ task: Task, _ selected: Bool) -> Void)) {
        self.checkedChangedHandler = checkedChangedHandler
        self.task = task

        if checkBox.superview == nil {
            addSubview(checkBox)
            addSubview(answerLabel)
            NSLayoutConstraint.activate([
                checkBox.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
                checkBox.heightAnchor.constraint(equalToConstant: 30),
                checkBox.widthAnchor.constraint(equalTo: checkBox.heightAnchor),
                checkBox.centerYAnchor.constraint(equalTo: answerLabel.centerYAnchor),

                answerLabel.leadingAnchor.constraint(equalTo: checkBox.trailingAnchor, constant: 8),
                answerLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -8),
                answerLabel.topAnchor.constraint(equalTo: topAnchor),
                answerLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
            ])
        }

        checkBox.style = .tick
        checkBox.borderStyle = .rounded
        let string = task.getName() as NSString
        let attributedString = NSMutableAttributedString(string: string as String)

        let boldRange = (string.lowercased as NSString).range(of: searchTerm.lowercased())
        let font = UIFont.boldSystemFont(ofSize: 20)

        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: boldRange)
        attributedString.addAttribute(.backgroundColor, value: UIColor.systemYellow, range: boldRange)
        answerLabel.attributedText = attributedString
        checkBox.isChecked = task.getFinished()
    }


    @objc
    func onCheckBoxValueChange(_ sender: CheckBox) {
        guard let task = task else { return }
        checkedChangedHandler?(task, sender.isChecked)
    }

    public func resetCheckBox() {
        checkBox.isChecked = false
    }
}


// swiftlint:disable all
open class CheckBox: UIControl {

        public enum CheckBoxStyle {
            case square
            case circle
            case cross
            case tick
        }

        /// Used as a visual indication of where the user can tap.
        public enum BorderStyle {
            case square
            case roundedSquare(radius: CGFloat)
            case rounded
        }

    var style: CheckBoxStyle = .circle
    var borderStyle: BorderStyle = .roundedSquare(radius: 8)

    @IBInspectable
    var borderWidth: CGFloat = 1.75

    var checkmarkSize: CGFloat = 0.5

    @IBInspectable
    var checkedBorderColor: UIColor = UIColor.blue

    @IBInspectable
    var checkmarkColor: UIColor = UIColor.blue

    @IBInspectable
    var uncheckedBorderColor: UIColor = UIColor.lightGray

    @IBInspectable
    var uncheckedColor: UIColor = UIColor.lightGray

    var checkboxBackgroundColor: UIColor! = .red

    //Used to increase the touchable are for the component
    var increasedTouchRadius: CGFloat = 5

    //By default it is true
    var useHapticFeedback: Bool = true

    @IBInspectable
    var isChecked: Bool = false {
        didSet{
            self.setNeedsDisplay()
        }
    }

    //UIImpactFeedbackGenerator object to wake up the device engine to provide feed backs
    private var feedbackGenerator: UIImpactFeedbackGenerator?

    //MARK: Intialisers
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }

    private func setupViews() {
        self.backgroundColor = .clear
    }

    //Define the above UIImpactFeedbackGenerator object, and prepare the engine to be ready to provide feedback.
    //To store the energy and as per the best practices, we create and make it ready on touches begin.
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.feedbackGenerator = UIImpactFeedbackGenerator.init(style: .light)
        self.feedbackGenerator?.prepare()
    }

    //On touches ended,
    //change the selected state of the component, and changing *isChecked* property, draw methos will be called
    //So components appearance will be changed accordingly
    //Hence the state change occures here, we also sent notification for value changed event for this component.
    //After usage of feedback generator object, we make it nill.
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        super.touchesEnded(touches, with: event)

        self.isChecked = !isChecked
        self.sendActions(for: .valueChanged)
        if useHapticFeedback {
            self.feedbackGenerator?.impactOccurred()
            self.feedbackGenerator = nil
        }
    }

    open override func draw(_ rect: CGRect) {
        //Draw the outlined component
        let newRect = rect.insetBy(dx: borderWidth / 2, dy: borderWidth / 2)
        let context = UIGraphicsGetCurrentContext()
        context?.setStrokeColor(self.isChecked ? checkedBorderColor.cgColor : uncheckedBorderColor.cgColor)
        context?.setFillColor(self.isChecked ? checkboxBackgroundColor.cgColor : uncheckedColor.cgColor)
        context?.setFillColor(checkboxBackgroundColor.cgColor)
        context?.setLineWidth(borderWidth)
        var shapePath: UIBezierPath!
        switch self.borderStyle {
        case .square:
            shapePath = UIBezierPath(rect: newRect)
        case .roundedSquare(let radius):
            shapePath = UIBezierPath(roundedRect: newRect, cornerRadius: radius)
        case .rounded:
            shapePath = UIBezierPath.init(ovalIn: newRect)
        }
        context?.addPath(shapePath.cgPath)
        context?.strokePath()
        context?.fillPath()
        //When it is selected, depends on the style
        //By using helper methods, draw the inner part of the component UI.
        if isChecked {
            switch self.style {
            case .square:
                self.drawInnerSquare(frame: newRect)
            case .circle:
                self.drawCircle(frame: newRect)
            case .cross:
                self.drawCross(frame: newRect)
            case .tick:
                self.drawCheckMark(frame: newRect)
            }
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.setNeedsDisplay()
    }

    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setNeedsDisplay()
    }

    //we override the following method,
    //To increase the hit frame for this component
    //Usaully check boxes are small in our app's UI, so we need more touchable area for its interaction
    open override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {

        let relativeFrame = self.bounds
        let hitTestEdgeInsets = UIEdgeInsets(top: -increasedTouchRadius, left: -increasedTouchRadius, bottom: -increasedTouchRadius, right: -increasedTouchRadius)
        let hitFrame = relativeFrame.inset(by: hitTestEdgeInsets)
        return hitFrame.contains(point)
    }

    //Draws tick inside the component
    func drawCheckMark(frame: CGRect) {

        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: frame.minX + 0.26000 * frame.width, y: frame.minY + 0.50000 * frame.height))
        bezierPath.addCurve(to: CGPoint(x: frame.minX + 0.42000 * frame.width, y: frame.minY + 0.62000 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.38000 * frame.width, y: frame.minY + 0.60000 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.42000 * frame.width, y: frame.minY + 0.62000 * frame.height))
        bezierPath.addLine(to: CGPoint(x: frame.minX + 0.70000 * frame.width, y: frame.minY + 0.24000 * frame.height))
        bezierPath.addLine(to: CGPoint(x: frame.minX + 0.78000 * frame.width, y: frame.minY + 0.30000 * frame.height))
        bezierPath.addLine(to: CGPoint(x: frame.minX + 0.44000 * frame.width, y: frame.minY + 0.76000 * frame.height))
        bezierPath.addCurve(to: CGPoint(x: frame.minX + 0.20000 * frame.width, y: frame.minY + 0.58000 * frame.height), controlPoint1: CGPoint(x: frame.minX + 0.44000 * frame.width, y: frame.minY + 0.76000 * frame.height), controlPoint2: CGPoint(x: frame.minX + 0.26000 * frame.width, y: frame.minY + 0.62000 * frame.height))
        checkmarkColor.setFill()
        bezierPath.fill()
    }

    func drawCircle(frame: CGRect) {
        //// General Declarations
        // This non-generic function dramatically improves compilation times of complex expressions.
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        //// Oval Drawing
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: frame.minX + fastFloor(frame.width * 0.22000 + 0.5), y: frame.minY + fastFloor(frame.height * 0.22000 + 0.5), width: fastFloor(frame.width * 0.76000 + 0.5) - fastFloor(frame.width * 0.22000 + 0.5), height: fastFloor(frame.height * 0.78000 + 0.5) - fastFloor(frame.height * 0.22000 + 0.5)))
        checkmarkColor.setFill()
        ovalPath.fill()
    }

    func drawInnerSquare(frame: CGRect) {

        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        let padding = self.bounds.width * 0.3
        let innerRect = frame.inset(by: .init(top: padding, left: padding, bottom: padding, right: padding))
        let rectanglePath = UIBezierPath.init(roundedRect: innerRect, cornerRadius: 3)

        checkmarkColor.setFill()
        rectanglePath.fill()
    }

    func drawCross(frame: CGRect) {
        let context = UIGraphicsGetCurrentContext()!
        func fastFloor(_ x: CGFloat) -> CGFloat { return floor(x) }
        //// Subframes
        let group: CGRect = CGRect(x: frame.minX + fastFloor((frame.width - 17.37) * 0.49035 + 0.5), y: frame.minY + fastFloor((frame.height - 23.02) * 0.51819 - 0.48) + 0.98, width: 17.37, height: 23.02)

        context.saveGState()
        context.translateBy(x: group.minX + 14.91, y: group.minY)
        context.rotate(by: 35 * CGFloat.pi/180)
        let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 3, height: 26))
        checkmarkColor.setFill()
        rectanglePath.fill()
        context.restoreGState()

        context.saveGState()
        context.translateBy(x: group.minX, y: group.minY + 1.72)
        context.rotate(by: -35 * CGFloat.pi/180)
        let rectangle2Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 3, height: 26))
        checkmarkColor.setFill()
        rectangle2Path.fill()
        context.restoreGState()
    }
}
// swiftlint:enable all

