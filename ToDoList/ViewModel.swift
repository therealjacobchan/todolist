//
//  ViewModel.swift
//  ToDoList
//
//  Created by Jacob Chan on 6/1/20.
//  Copyright © 2020 GCash. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

public class ViewModel {
    private var tasks = BehaviorRelay<[Task]>(value: [])
    private var filteredTasks = BehaviorRelay<[Task]>(value: [])

    public var getTasksSuccessfully: ((_ response: Bool, _ issue: String) -> Void)? = nil
    public var modifyTaskSuccessfully: ((_ response: Bool, _ issue: String) -> Void)? = nil
    public var searchTerm = ""

    public init() {
        extractTasks()
    }

    public func getTask(index: Int) -> Task {
        return self.filteredTasks.value[index]
    }

    public func getTasks() -> BehaviorRelay<[Task]> {
        return filteredTasks
    }

    public func deleteTask(task: Task) {
        let index = self.tasks.value.firstIndex(of: task) ?? 0
        self.tasks.remove(at: index)
        self.filteredTasks.removeAll()
        self.filteredTasks.accept(self.tasks.value)
        let output = self.storeTask()
        modifyTaskSuccessfully?(output, output ? "" : "Something went wrong with deleting the task.")
    }

    public func editTask(index: Int, task: Task) {
        if task.getName().isEmpty {
            modifyTaskSuccessfully?(false, "Task should not be empty!")
            return
        }
        self.tasks.insert(task, at: index)
        self.tasks.remove(at: index+1)
        self.filteredTasks.removeAll()
        self.filteredTasks.accept(self.tasks.value)
        let output = self.storeTask()
        modifyTaskSuccessfully?(output, output ? "" : "Something went wrong with editing the task.")
    }

    public func saveTask(task: Task) {
        if task.getName().isEmpty {
            modifyTaskSuccessfully?(false, "Task should not be empty!")
            return
        }
        self.tasks.accept(self.tasks.value + [task])
        self.filteredTasks.removeAll()
        self.filteredTasks.accept(self.tasks.value)
        let output = self.storeTask()
        modifyTaskSuccessfully?(output, output ? "" : "Something went wrong with saving the task.")
    }

    public func filterResults(text: String) {
        self.searchTerm = text
        if text.isEmpty {
            self.filteredTasks.removeAll()
            self.filteredTasks.accept(self.tasks.value)
            return
        }
        let value = self.tasks.value.filter { $0.getName().lowercased().contains(text.lowercased()) }
        self.filteredTasks.removeAll()
        self.filteredTasks.accept(value)
    }

    //private functions
    private func extractTasks() {
        guard let tasksData = UserDefaults.standard.object(forKey: "tasks") as? NSData else {
            return
        }

        do {
            if let tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(tasksData as Data) as? [Task] {
                self.tasks.accept(tasks)
                self.filteredTasks.accept(tasks)
                getTasksSuccessfully?(true, "")
            } else {
                getTasksSuccessfully?(false, "Something went wrong with task extraction.")
            }
        } catch {
            getTasksSuccessfully?(false, "Something went wrong with task extraction.")
        }
    }

    private func storeTask() -> Bool {
        do {
            let taskData = try NSKeyedArchiver.archivedData(withRootObject: self.tasks.value, requiringSecureCoding: false)
            UserDefaults.standard.set(taskData, forKey: "tasks")
            return true
        } catch {
            return false
        }
    }
}

extension BehaviorRelay where Element: RangeReplaceableCollection {
    func insert(_ subElement: Element.Element, at index: Element.Index) {
        var newValue = value
        newValue.insert(subElement, at: index)
        accept(newValue)
    }

    func remove(at index: Element.Index) {
        var newValue = value
        newValue.remove(at: index)
        accept(newValue)
    }

    func removeAll() {
        var newValue = value
        newValue.removeAll()
        accept(newValue)
    }
}
